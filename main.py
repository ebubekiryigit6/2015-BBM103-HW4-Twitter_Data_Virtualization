# -*- coding: utf-8 -*-

import json
import string
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
from operator import itemgetter
#
###
#####
#######

#you can fix it

FILE_NAME="tweet_data.json"

#######
#####
###
#
d={}#string counter func
def counter_list(list,list1):
    for i in list:
        if not i in list1:
            if i in d:
                d[i]=d[i]+1
            else:
                d[i]=1


d1={}
e={}
g={}
h={}
m={}
j={}
k={}


def counter_v3(list,string1,dict2):   #a string counter per minute
    a = unicode(dt.date())+" "+unicode(dt.hour*60+dt.minute)
    for i in list:
        if i == string1:
            if not a in dict2:
                dict2[a]={string1:int(1)}
            else:
                dict2[a][string1]+=1


def count_together(list,list2,matrix):  #str1 and str2 in tweet? count++
    for j in range(10):
        for i in range(len(list)):
            if list[j][i][0] in list2:
                if list[j][i][1] in list2:
                    matrix[j][i]+=1


def time_dict(dict):  #a dict which stored times
    a12=dt.hour*60+dt.minute
    if a12 in dict:
        dict[a12]+=1
    else:
        dict[a12]=1


#print top n string(for writting code easily)
def top_n(dict1,n):
    for i in range(0,n):
        print i+1,"-)" ,dict1[i]


#PART 1    PART 1    PART 1    PART 1    PART 1   PART 1

punctuation = list(string.punctuation)
stop = punctuation+[ 'a','an','the','rt', 'via','to','of','for','and','or','i','in','at','on','out','with','by','de',' ','is','am','are','my','your','our','us','me','you','it','','the','no','have','has','we','her','his','them','when','who','where','which','how','that','not','this','&amp;','from','new','la','but']

f=open(FILE_NAME, 'r') #file opens to read tweets and count words
for line in f:
    tweet = json.loads(line) # load it as Python dict
    created_at_format = '%a %b %d %H:%M:%S +0000 %Y'
    str=tweet['text']#tweet
    str=str.lower()
    L=str.split(" ")
    date=tweet["created_at"]#date-time in ISO format
    dt=datetime.strptime(date, "%a %b %d %H:%M:%S +0000 %Y") #reading date in ISO format
    counter_list(L,stop)
    time_dict(d1)



#after counting words, sort them
sorted_c=(sorted(d.items(), key=itemgetter(1),reverse=True))
#print sorted_c


#PART 2     PART 2     PART 2   #PART 2     PART 2     PART 2

f2=open(FILE_NAME, 'r') #file opens to count per minute
for line in f2:
    tweet = json.loads(line) # load it as Python dict
    created_at_format = '%a %b %d %H:%M:%S +0000 %Y'
    str=tweet['text']#tweet
    str=str.lower()
    L=str.split(" ")
    date=tweet["created_at"]#date-time in ISO format
    dt=datetime.strptime(date, "%a %b %d %H:%M:%S +0000 %Y") #reading date in ISO format
    counter_v3(L,unicode(sorted_c[0][0]),e)
    counter_v3(L,unicode(sorted_c[1][0]),g)
    counter_v3(L,unicode(sorted_c[2][0]),h)
    counter_v3(L,unicode(sorted_c[3][0]),m)
    counter_v3(L,unicode(sorted_c[4][0]),j)

sorted_c=(sorted(d.items(), key=itemgetter(1),reverse=True))
#print sorted_c
#print "\n"


g1=sorted(g.items())
e1=sorted(e.items())
h1=sorted(h.items())   #sort the dicts
m1=sorted(m.items())
j1=sorted(j.items())

#print g1
#print e1
#print h1
#print m1
#print j1


#print("\n")

#top_n(sorted_c,20)   #for writting code easily



f3=open("term_frequencies.txt","w")
for i in range(20):
    a4=sorted_c[i][0]
    a5=unicode(sorted_c[i][1])
    f3.write(a4)
    f3.write(" ")
    f3.write(a5)
    f3.write("\n")
f3.close()

f4=open("term_frequencies_overtime.txt","w")
for i in range(len(e1)):
    f4.write(e1[i][1].keys()[0])
    f4.write(" ")
    f4.write(e1[i][0])
    f4.write(" ")
    f4.write(" ")
    f4.write(unicode(e1[i][1].values()[0]))
    f4.write("\n")
for i in range(len(g1)):
    f4.write(g1[i][1].keys()[0])
    f4.write(" ")
    f4.write(g1[i][0])
    f4.write(" ")
    f4.write(" ")
    f4.write(unicode(g1[i][1].values()[0]))
    f4.write("\n")
for i in range(len(h1)):
    f4.write(h1[i][1].keys()[0])
    f4.write(" ")
    f4.write(h1[i][0])
    f4.write(" ")
    f4.write(" ")
    f4.write(unicode(h1[i][1].values()[0]))
    f4.write("\n")
for i in range(len(m1)):
    f4.write(m1[i][1].keys()[0])
    f4.write(" ")
    f4.write(m1[i][0])
    f4.write(" ")
    f4.write(" ")
    f4.write(unicode(m1[i][1].values()[0]))
    f4.write("\n")
for i in range(len(j1)):
    f4.write(j1[i][1].keys()[0])
    f4.write(" ")
    f4.write(j1[i][0])
    f4.write(" ")
    f4.write(" ")
    f4.write(unicode(j1[i][1].values()[0]))
    f4.write("\n")
f.close()




#PART 3    PART 3     PART 3    PART 3     PART 3

#top n list
a00=[]
def top_n_list(dict5,list,n):
    for i in range(0,n):
        list.append(dict5[i][0])
top_n_list(sorted_c,a00,10)
#print a00

#define matrix 10x10
matrix1=[[0]*10 for i in range(10)]
#print matrix1


top10_list=[sorted_c[i][0] for i in range(10)]   #define list which stored top10 word
#print top10_list

comb_list=[[(j,i) for i in top10_list] for j in top10_list]   #all of words combination list
#print comb_list

f6=open(FILE_NAME,"r")
for line in f6:
    tweet = json.loads(line) # load it as Python dict
    created_at_format = '%a %b %d %H:%M:%S +0000 %Y'
    str=tweet['text']#tweet
    str=str.lower()
    L=str.split(" ")
    date=tweet["created_at"]#date-time in ISO format
    dt=datetime.strptime(date, "%a %b %d %H:%M:%S +0000 %Y") #reading date in ISO format
    count_together(comb_list,L,matrix1)

matrix_1=np.matrix(matrix1) #crate a numpy array
#print matrix_1

f5=open("term_cooccurrences.txt","w")
for j in range(len(matrix1)):
    for i in range(len(comb_list)):
        f5.write(comb_list[j][i][0])
        f5.write("-")
        f5.write(comb_list[j][i][1])
        f5.write(" ")
        f5.write(unicode(matrix1[j][i]))
        f5.write("\n")
f5.close()






#PLOTS   PLOTS   PLOTS   PLOTS   PLOTS



##  PLOT PART1     PLOT PART1  PLOT PART1
a=[]
b=[]
for i in range(20):
    a.append(sorted_c[i][1])
c=np.array(a)
for i in range(20):
    b.append(sorted_c[i][0])
q=np.array(b)
t=[i-0.5 for i in range(1,21)]

#part 1 histogram
fig2=plt.figure()
plt.ylim(0,2000)
plt.xlim(0,20)
plt.ylabel("Term Frequencies")
plt.title("Tweet Miner")
plt.bar(range(20),c,facecolor='blue')
plt.xticks(t,q,fontsize=8)
plt.savefig("term_frequencies.png")
#plt.show()



#PLOT PART2    PLOT PART2    PLOT PART2
a2=[]
for i in range(len(e1)):
    a2.append(e1[i][1].values()[0])
#print a2

a1=[]
for i in range(len(g1)):
    a1.append(g1[i][1].values()[0])
#print a1

a3=[]
for i in range(len(h1)):
    a3.append(h1[i][1].values()[0])
#print a3

a4=[]
for i in range(len(m1)):
    a4.append(m1[i][1].values()[0])
#print a4

a5=[]
for i in range(len(j1)):
    a5.append(j1[i][1].values()[0])
#print a5



#part 2 graph
b1=sorted_c[0][0]
b2=sorted_c[1][0]
b3=sorted_c[2][0]
b4=sorted_c[3][0]
b5=sorted_c[4][0]
fig3=plt.figure()
plt.ylim(0,200)
plt.xlim(0,22)
plt.plot(range(len(a1)),a1,label=b1)
plt.plot(range(len(a2)),a2,label=b2)
plt.plot(range(len(a3)),a3,label=b3)
plt.plot(range(len(a4)),a4,label=b4)
plt.plot(range(len(a5)),a5,label=b5)
plt.grid(True)
plt.ylabel("Frequency of Occurrence")
plt.xlabel("Time")
a2=[i for i in range(0,23)]
a0=[d1.keys()[i] for i in range(len(d1))]
plt.legend((e1[0][1].keys()[0],g1[0][1].keys()[0],h1[0][1].keys()[0],m1[0][1].keys()[0],j1[0][1].keys()[0]))
plt.xticks(a2,a0)
plt.savefig("term_frequencies_overtime.png")
#plt.show()



#part3 graph

fig4 = plt.figure()
ax = fig4.add_subplot(1,1,1)
ax.set_aspect('equal')
plt.imshow(matrix_1, interpolation='nearest', cmap=plt.cm.seismic)
plt.grid(True)
plt.colorbar()
plt.savefig("term_cooccurrences.png")
plt.show()



print "\n","Ebubekir Yigit__//"




